<?php

namespace NoTeam\UserExtension\Tests;

use Orchestra\Database\ConsoleServiceProvider;
use Illuminate\Contracts\Console\Kernel as ConsoleKernel;
use Illuminate\Foundation\Testing\WithFaker;
use NoTeam\UserExtension\Tests\Model\User;
use Illuminate\Database\Schema\Blueprint;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    use WithFaker;

    protected function setUp()
    {
        parent::setUp();
        $this->loadLaravelMigrations();
        $this->loadMigrationsFrom(realpath(__DIR__.'/database/migrations'));
        $this->seedUser();
    }

    protected function getPackageProviders($app)
    {
        return [
            'NoTeam\UserExtension\UserExtensionServiceProvider',
            'Orchestra\Database\ConsoleServiceProvider',
        ];
    }

    protected function getApplicationTimezone($app)
    {
        return 'Asia/Jakarta';
    }
    
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    protected function setUpDatabase($app)
    {
        $app['db']->connection()->getSchemaBuilder()->table('products', function (Blueprint $table) {
            $table->string('name');
        });
    }

    protected function seedUser()
    {
        $user = new User();
        $user->name = $this->faker->name;
        $user->email = $this->faker->email;
        $user->password = \Hash::make('secret');

        $user->save();
    }
}