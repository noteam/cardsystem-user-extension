<?php

namespace NoTeam\UserExtension\Tests\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use NoTeam\UserExtension\VerifiableInterface;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements  VerifiableInterface
{
    use Notifiable;

    public function getToken()
    {
        return $this->token;
    }

    public function setToken(string $token)
    {
        $this->token = $token;
        return $this;
    }

    public function getEmailField()
    {
        return $this->email;
    }

    public function setExpired(\DateTimeInterface $expiredAt)
    {
        $this->expired_at = $expiredAt;
        return $this;
    }

    public function getExpired()
    {
        return $this->expired_at;
    }

    public function setVerifiedAt(\DateTimeInterface $verifiedAt)
    {
        $this->verified_at = $verifiedAt;
        return $this;
    }

    public function getVerifiedAt()
    {
        return $this->verified_at;
    }

    public function sendEmailVerification()
    {
        app('noteam.user-verification.email')->sendVerification($this);
    }
}