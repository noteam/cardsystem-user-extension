<?php

namespace NoTeam\UserExtension;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Str;
use NoTeam\UserExtension\Notifications\EmailVerifyNotification;

class EmailVerification
{
    private $userProvider;

    private $expiration = 30; // in minutes;

    private $mailView = 'email.user.verification';

    private $notificationClass;

    public function __construct(UserProvider $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    public function createToken()
    {
        return hash_hmac('sha256', Str::random(40), env('APP_KEY'));
    }

    public function setMailView(string $mailView)
    {
        $this->mailView = $mailView;
        return $this;
    }

    public function getMailView()
    {
        return $this->mailView;
    }

    public function setExpiration(int $expiration)
    {
        $this->expiration = $expiration;
        return $this;
    }

    public function getExpiration()
    {
        return $this->expiration;
    }

    public function setNotificationClass($notificationClass)
    {
        $this->notificationClass = $notificationClass;
        return $this;
    }

    public function getNotificationClass()
    {
        if ($this->notificationClass) {
            return $this->notificationClass;
        }

        return $this->notificationClass = EmailVerifyNotification::class;
    }

    public function sendVerification(VerifiableInterface $user)
    {
        $token = $this->createToken();
        $expiredAt = now()->addMinutes($this->getExpiration());

        $user->setToken($token);
        $user->setExpired($expiredAt);
        $user->save();

        $notificationClass = $this->getNotificationClass();
        $notificationClass = new $notificationClass;
        $notificationClass->setToken($token);

        $user->notify($notificationClass);
    }

    public function isVerified(VerifiableInterface $user)
    {
        if ($user->getVerifiedAt()) {
            return true;
        }
        return false;
    }

    public function verify(VerifiableInterface $user)
    {
        $user->setVerifiedAt(now());
        return $user->save();
    }
}